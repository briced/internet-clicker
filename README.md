# Data clicker

Testing how to implement a basic cookie clicker (http://orteil.dashnet.org/cookieclicker/) with RxJS (5.5 with pipes) and webpack 4 (beta).

## How to build.

Clone the project, then run 'npm run build'

Want to modify it? Run 'npm start' (livereload with webpack. Doesn't work for now, because webpack-dev-server is not ready for webpack beta 4)

## Thanks

* https://medium.com/webpack/webpack-4-beta-try-it-today-6b1d27d7d7e2
* https://blog.hackages.io/rxjs-5-5-piping-all-the-things-9d469d1b3f44
