import { Observable, pipe, fromEvent } from 'rxjs/Rx'
import { mapTo, scan } from 'rxjs/operators'
import { merge } from 'rxjs/observable/merge'
import { interval } from 'rxjs/observable/interval'

const game = document.getElementById('game')

const createButton = (name, initialValue = 1) => {
  const newButton = document.createElement('button')
  const textSpan = document.createElement('span')
  textSpan.innerHTML = `${name} `
  const valueSpan = document.createElement('span')
  valueSpan.innerHTML = initialValue
  newButton.appendChild(textSpan)
  newButton.appendChild(valueSpan)
  return newButton
}

const liForButton = button => {
  const newButtonInLi = document.createElement('li')
  newButtonInLi.appendChild(button)
  return newButtonInLi
}

// Let's add a 'total' field
const total = document.createElement('span')
total.innerHTML = 'Total: '
const totalValue = document.createElement('span')
total.appendChild(totalValue)
const unit = document.createElement('span')
unit.innerHTML = ' bits'
total.appendChild(unit)

game.appendChild(total)
// Let's add a list for buttons
const buttonList = document.createElement('ul')

// And the buttons
const manualButton = createButton('Create bit')
buttonList.appendChild(liForButton(manualButton))

const simpleButton = createButton('56k')
buttonList.appendChild(liForButton(simpleButton))
const mediumButton = createButton('DSL', 5)
buttonList.appendChild(liForButton(mediumButton))
const highButton = createButton('Fiber', 10)
buttonList.appendChild(liForButton(highButton))

game.appendChild(buttonList)
// DOM is ready. Now, time for mapping

// Basic tick
const tick$ = interval(1000)

const manual$ = Observable.fromEvent(manualButton, 'click')

const simple$ = Observable.fromEvent(simpleButton, 'click')
const medium$ = Observable.fromEvent(mediumButton, 'click')
const high$ = Observable.fromEvent(highButton, 'click')

// Debug
// simple$.subscribe(s => console.log(`Click Simple: ${s}`));
// medium$.subscribe(s => console.log(`Click Medium: ${s}`));
// high$.subscribe(s => console.log(`Click High: ${s}`));

const scanSum = scan((acc, next) => acc + next, 0)

const manualMapped$ = manual$.pipe(mapTo(1))
const simpleMapped$ = simple$.pipe(mapTo(1))
const mediumMapped$ = medium$.pipe(mapTo(5))
const highMapped$ = high$.pipe(mapTo(10))

// Debug
// simpleMapped$.subscribe(s => console.log(`Simple: ${s}`));
// mediumMapped$.subscribe(s => console.log(`Medium: ${s}`));
// highMapped$.subscribe(s => console.log(`High: ${s}`));

const totalSum$ = merge(simpleMapped$, mediumMapped$, highMapped$).pipe(scanSum)

// Debug
// totalSum$.subscribe(s => console.log(`TotalSum: ${s}`));

tick$
  .withLatestFrom(totalSum$, (tick, sum) => sum)
  .merge(manualMapped$)
  .pipe(scanSum)
  .subscribe(total => {
    totalValue.innerHTML = total
  })

console.log(totalSum$)
