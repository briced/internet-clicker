// JQuery for DOM manipulation
import $ from "jquery";
import Rx from "rxjs/Rx";

// All is done in JS
document.title = "Internet Clicker";

// Basic tick
const tick$ = Rx.Observable.interval(100);

// Helper
const toCountObservable = domObservable => {
  return domObservable.mapTo(1).scan((acc, one) => acc + one, 0);
};

const _createButton = clickable => {
  return (label, cookie$, initValue, multiplier) => {
    let value = 0;
    const value$ = new Rx.Subject();
    const clickButton = $(`<span></span>`);

    const _renderButton = (clickable, value) =>
      clickable
        ? clickButton.html(
            `<input type="button" value="${label} (${value})"></input>`
          )
        : clickButton.html(`<span>${label} (${value})</span>`);

    value$.subscribe(value => _renderButton(clickable, value.toFixed(2)));

    value$.next(initValue);

    const upgradeButton = $(`<input type="button" value="upgrade!"></input>`);
    // TODO disabled if cookies number is too low
    $(document.body)
      .append("<div>")
      .append(clickButton)
      .append(upgradeButton)
      .append("</div>");

    value$.subscribe(val => (value = val));
    const clickButton$ = clickable
      ? Rx.Observable.fromEvent(clickButton, "click")
      : Rx.Observable.empty();
    const upgradeButton$ = Rx.Observable.fromEvent(upgradeButton, "click");
    upgradeButton$.subscribe(ev => value$.next(value * multiplier + 1));

    const clickvalue$ = clickable
      ? Rx.Observable.combineLatest(clickButton$, value$, (c, val) => val)
      : Rx.Observable.combineLatest(tick$, value$, (t, val) => val / 10);
    return [clickButton$, upgradeButton$, clickvalue$];
  };
};

const clickableButton = _createButton(true);
const unclickableButton = _createButton(false);

// Init
let clickValue = 1;
let farmDifficulty = {
  small: 1.1,
  medium: 1.5,
  big: 2,
  ultimate: 5
};

let cookie$ = Rx.Observable.empty();

// Main 'Click' button
const [clickButton$, upgradeClickButton$, clickValue$] = clickableButton(
  "Farm the Internet!",
  cookie$,
  clickValue,
  2
);

// Automatic builder
const [smallFarmClick$, smallFarmUpgrade$, smallFarmValue$] = unclickableButton(
  "Small automatic farm",
  cookie$,
  0,
  farmDifficulty.small
);
const [
  mediumFarmClick$,
  mediumFarmUpgrade$,
  mediumFarmValue$
] = unclickableButton(
  "Medium automatic farm",
  cookie$,
  0,
  farmDifficulty.medium
);
const [bigFarmClick$, bigFarmUpgrade$, bigFarmValue$] = unclickableButton(
  "Big automatic farm",
  cookie$,
  0,
  farmDifficulty.big
);
const [
  ultimateFarmClick$,
  ultimateFarmUpgrade$,
  ultimateFarmValue$
] = unclickableButton(
  "Ultimate automatic farm",
  cookie$,
  0,
  farmDifficulty.ultimate
);

const clickCountHtml = $("<span></span>");
const cookieCountHtml = $("<span></span>");
$(document.body).append(
  $("<div>")
    .append("<span>Cookies count: </span>")
    .append(cookieCountHtml)
    .append("(Clicks count:")
    .append(clickCountHtml)
    .append(")")
);

// Total clicks count
const allClicks$ = Rx.Observable.merge(
  clickButton$,
  upgradeClickButton$,
  smallFarmClick$,
  smallFarmUpgrade$,
  mediumFarmClick$,
  mediumFarmUpgrade$,
  bigFarmClick$,
  bigFarmUpgrade$,
  ultimateFarmClick$,
  ultimateFarmUpgrade$
);

toCountObservable(allClicks$).subscribe(x => clickCountHtml.html(x));

cookie$ = Rx.Observable.merge(
  clickValue$,
  smallFarmValue$,
  mediumFarmValue$,
  bigFarmValue$,
  ultimateFarmValue$
);
cookie$
  .scan((acc, value) => acc + value, 0)
  .subscribe(x => cookieCountHtml.html(Math.floor(x)));
